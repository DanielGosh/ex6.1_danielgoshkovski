#include <iostream>
int add(int a, int b) {
	if (a + b == 8200)
	{
		throw "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year";
	}
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
	};
	if (sum == 8200)
	{
		throw "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year";
	}
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
	};
	if (exponent == 8200)
	{
		throw "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year";
	}
	return exponent;
}

int main(void) {
	int a = 0, b = 0;
	int answer = 0;
	int choice = 0;
	std::cout << "enter a and b values: ";
	std::cin >> a;
	std::cin >> b;
	std::cout << "--------\n[1]add\n[2]multiply\n[3]exponent\n--------\n";
	std::cin >> choice;
	if (choice == 1)
	{
		try
		{
			answer = add(a, b);
		}
		catch (const char* msg)
		{
			std::cout << msg << "\n";
		}
	}
	else if (choice == 2)
	{
		try {
			answer = multiply(a, b);
		}
		catch (const char* msg)
		{
			std::cout << msg << "\n";
		}
	}
	else if (choice == 3)
	{
		try {
			answer = pow(a, b);
		}
		catch (const char* msg)
		{
			std::cout << msg << "\n";
		}
	}
	else
	{
		std::cout << "illegal action\n";
	}
	std::cout << "the answer to your action = " << answer;
	return 0;
}