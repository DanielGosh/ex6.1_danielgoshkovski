#include <iostream>
int add(int a, int b) {
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
	};
	return exponent;
}

int main(void)
{
	int a = 0, b = 0;
	int answer = 0;
	int choice = 0;
	std::cout << "enter a and b values: ";
	std::cin >> a;
	std::cin >> b;
	std::cout << "--------\n[1]add\n[2]multiply\n[3]exponent\n--------\n";
	std::cin >> choice;
	if (choice == 1)
	{
		answer = add(a, b);
	}
	else if (choice == 2)
	{
		answer = multiply(a, b);
	}
	else if (choice == 3)
	{
		answer = pow(a, b);
	}
	else
	{
		std::cout << "illegal action\n";
	}
	if (answer == 8200)
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year";
	}
	else
	{
		std::cout << "the answer to your action = " << answer;
	}
	return 0;
}